#!/bin/bash
#######################################################################
## Hilfe:
##
## http://www.linuxjournal.com/content/return-values-bash-functions
## http://wiki.ubuntuusers.de/Shell/Bash-Skripting-Guide_f%C3%BCr_Anf%C3%A4nger
##
## ToDo
## Die Dateinamen selber wählen und ausgeben und anschliessend 
## eingeben
##
## Prgrammierer: Simon Wittwer
## Datum: 09.12.2014
##
#######################################################################
## Version 0.4                                                       ##
## Der Benutzer kann seinen Account löschen. Wenn er dies macht      ##
## werden alle seine Daten gelöscht.                                 ##
## ------------------------------------------------------------------##
## Version 0.3                                                       ##
## Das Grundgerüst für einen Administartor Account erstellt. Bis     ##
## kann aber noch nichts durchgeführt werden. Nur an- und abmelden   ##
## ist möglich.                                                      ##
## ------------------------------------------------------------------##
## Version 0.2                                                       ##
## Es ist möglich dass verschiedene Benutzer verschidene Tagebücher  ##
## sehen können                                                      ##
## ------------------------------------------------------------------##
## Version 0.1                                                       ##
## Das Grundgerüst hat funktioniert. Es kann geschrieben und wieder  ##
## gelöscht werden                                                   ##
#######################################################################
## Variablen
##
wurzelVerz="$HOME/tagebuchfuerdebian"
passwortDatei="$HOME/tagebuchfuerdebian/data/passwd"
passwortTempDatei="$HOME/tagebuchfuerdebian/data/temp"
badWords="$HOME/tagebuchfuerdebian/data/badwords"
adminChef="$HOME/tagebuchfuerdebian/data/admin"
geschichtenPfad="$HOME/tagebuchfuerdebian/Geschichten/"
versionVar="Version 0.3.2"
# Hier schreibe ich einfach etwas rein um so zu tun als würde ich arbeiten.
## 
## Funktionen
##
# Statemachine - nur eine hilfsfunktion
kopfzeile() {
             #Uhrzeit Variable
             datumHeute=$(date +"%d.%m.%y")
             echo "+----------------------------------------+"
             echo "| Tagebuch Script | $versionVar "
             echo "| Wir haben $datumHeute"
             # Neu in Version 0.2
             echo "| Angemeldet als: $benutzerTagebuch"
             echo "+----------------------------------------+"
             }

# Statemachine 7
loginFenster() {
                echo "+----------------------------------------+"
                echo "| Bitte melden Sie sich als Benutzer an"
                echo "| oder erstellen Sie einen neuen Name"
                echo "+----------------------------------------+"
                echo "| 1  --> Login                           "
                echo "| 2  --> Registrieren                    "
                echo "| 4  --> Administrator                   "
                echo "| 5  --> Beenden                         "
                echo "+----------------------------------------+"
                read -p "Eingabe > " auswahl
                if [[ $auswahl == 1 ]]; then statemachine=8; fi
                if [[ $auswahl == 2 ]]; then statemachine=9; fi
                if [[ $auswahl == 4 ]]; then statemachine=11; fi
                if [[ $auswahl == 5 ]]; then statemachine=99; fi
                return statemachine
               }

# Statemachine 8
loginLogin() {
               echo "+----------------------------------------+"
               echo "| Bitte geben Sie ihren Benutzername an  "
               echo "+----------------------------------------+"
               read -p "Eingabe > " tagebuchBenutzer
               failORnot=$(grep -o "$tagebuchBenutzer" "$passwortDatei" | wc -l)
                if [[ $failORnot == 1 ]]; then statemachine=1; benutzerTagebuch=$tagebuchBenutzer #Login korrekt
                   cd "$geschichtenPfad"$benutzerTagebuch
                   echo "Login erfolgreich! ... "
                   read
                  else 
                   echo "Login fehlgeschlagen"
                   read
                   statemachine=7 # Login fehlgeschlagen zurueck ins loginFenster
                fi
               return $statemachine
               }

# Statemachine 
loginSicherheit() {
                   echo "Hallo Welt"
                   }

# Statemachine 9
loginRegistrieren() {
                     echo "+----------------------------------------+"
                     echo "| Bitte Registieren Sie sich             "
                     echo "| Waehlen Sie einen Benutzername aus     "
                     echo "+----------------------------------------+"
                     read -p "Eingabe > " neuerBenutzername
                     # badORnot=$(grep -o "$neuerBenutzername" "$badWords" | wc -l)
                     # if [[ $badORnot > 0 ]]; then echo "Dieser Benutzername ist gesperrt ..."; $statemachine=9
                     #  read
                     # return $statemachine
                     #fi
                     freiODERnicht=$(grep -o "$neuerBenutzername" "$passwortDatei" | wc -l)
                     if [[ $freiODERnicht == 0 ]]; then echo "Benutzername noch frei, er wird gespeichert ... "
                        echo -e "$neuerBenutzername\n" >> $passwortDatei
                        cd "$geschichtenPfad"
                        mkdir "$neuerBenutzername" #Verzeichnis erstellen
                        cd "$wurzelVerz"
                        read
                        statemachine=7
                     else 
                         echo "Benutzername bereits vergeben! Bitte einen anderen waehlen"
                      read
                     statemachine=9
                     fi
                     return $statemachine
                     }

# Statemachine 
benutzerLoeschen() {
                    kopfzeile
                    echo "| Benutzername löschen                          "
                    echo "| Es werden alle Geschichten von dir gelöscht!  "
                    echo "+----------------------------------------------+"
                    echo "| Willst du deinen Account $benutzerTagebuch    "
                    echo "| wirklich löschen?                             "
                    echo "|"
                    echo "| Ja oder Nein eintippen!                       "
                    echo "+----------------------------------------------+"
                    read -p "Deine Eingabe > " auswahl
                     if [[ $auswahl =~ ^([jJ][aA]|[jJ])$ ]]; then 
                       # Geschichten löschen
                       cd "$geschichtenPfad"
                       rm -r "$benutzerTagebuch"
                       # Geschichte löschen ende
                       # Benutzer aus der Datei entfernen
                       grep -v "$benutzerTagebuch" "$passwortDatei" > "$passwortTempDatei"
                       mv "$passwortTempDatei" "$passwortDatei"
                       #rm "$passwortTempDatei"
                       # Benutzer aus der Datei entfernen fertig
                       echo "Fertig ... "
                       read
                       statemachine=7
                     fi
                     if [[ $auswahl =~ ^([nN][eE][iI][nN]|[nN])$ ]]; then 
                      echo "Abgebrochen ... "
                      read
                      statemachine=1
                     fi
                     return $statemachine
                    }

# Statemachine 1
hauptmenue() {
              kopfzeile #Funktion kopfzeile()
              echo "| 1  --> Geschichte / Tagebuch           "
              echo "| 2  --> Ausloggen                       "
              echo "| 5  --> Benutzername löschen            "
              #echo "| 99 --> Programm beenden                "
              echo "+----------------------------------------+"
              read -p "Eingabe > " auswahl
              if [[ $auswahl == 1 ]]; then statemachine=4; fi
              if [[ $auswahl == 2 ]]; then benutzerTagebuch=" "; statemachine=7; fi
              if [[ $auswahl == 5 ]]; then statemachine=10; fi
              #if [[ $auswahl == 99 ]]; then statemachine=99; fi
              return $statemachine
             }

# Statemachine 4
geschichte() {
              kopfzeile #Funktion kopfzeile()
              echo "| Sie befinden sich im Menü Geschichte   "
              echo "+----------------------------------------+"
              echo "| 1  --> Geschichte schreiben            "
              echo "| 2  --> Geschichte lesen                "
              echo "| 3  --> Geschichte löschen              "
              echo "| 4  --> Geschichte umnennen             "
              echo "| 10 --> Hauptmenü                       "
              echo "+----------------------------------------+"
              read -p "Eingabe > " auswahl
              if [[ $auswahl == 1 ]]; then statemachine=5; fi
              if [[ $auswahl == 2 ]]; then statemachine=6; fi 
              if [[ $auswahl == 3 ]]; then statemachine=4.1; fi
              if [[ $auswahl == 4 ]]; then statemachine=4.2; fi
              if [[ $auswahl == 10 ]]; then statemachine=1; fi
              return $statemachine
             }

# Statemachine 4.1
geschichteLoeschen() {
                      echo "| Geschichten löschen                    "
                      echo "+----------------------------------------+"
                      geschichteAuflisten
                      echo "| 1  --> zurück zu Geschichte            "
                      echo "+----------------------------------------+"
                      read -p "Eingabe > " auswahl
                      if [[ $auswahl == 1 ]]; then statemachine=4; return $statemachine; fi
                      if [[ $auswahl =~ ^$varAbisZ ]]
                       then #read -p "Name der zu löschenden Geschichte! > " auswahlZwei
                            rm "$geschichtenPfad""$benutzerTagebuch"/"$auswahl"; statemachine=4; fi
                            echo "Datei wurde gelöscht!"; read
                      return $statemachine
                     }

# Statemachine 4.2
geschichteUmnennen() {
                      echo "| Geschichten umnennen                   "
                      echo "+----------------------------------------+"
                      geschichteAuflisten
                      echo "| 1 --> zurück zu Geschichte             "
                      echo "+----------------------------------------+"
                      echo "| Bezeichnung der Geschichte eingegeben  "
                      echo "+----------------------------------------+"
                      read -p "Eingabe > " auswahl
                      if [[ $auswahl == 1 ]]; then statemachine=4; return $statemachine; fi
                      if [[ $auswahl =~ ^$varAbisZ ]]
                       then read -p "Neuer Name > " auswahlZwei
                            mv "$geschichtenPfad""$benutzerTagebuch"/"$auswahl" "$geschichtenPfad""$benutzerTagebuch"/"$auswahlZwei"; statemachine=4; fi
                      return $statemachine
                     }

# Statemachine 5
geschichteSchreiben() {
                       kopfzeile
                       echo "| Hier kannst du schreiben!              "
                       echo "+----------------------------------------+"
                       varGeschichte=""
                       read -p "| Schreibe ... " varGeschichte
                       # Wenn gleicher Name eingegeben wird, wird die Geschichte um den Text erweitert
                       read -p "| Name der Geschichte Eingeben > " varGeschichteName
                       # Wenn Geschichte bereits vorhanden überschreiben oder neue erstellen
                       echo $varGeschichte >> "$geschichtenPfad""$benutzerTagebuch"/$varGeschichteName
                       echo " Die Geschichte wurde gesepichert ..."
                       read
                       statemachine=4
                       return $statemachine
                       }

# Statemachine 6
geschichteLesen() {
                    echo "| Geschichten lesen                      "
                    echo "+----------------------------------------+"
                    geschichteAuflisten
                    echo "| 1  --> Neue Geschichte schreiben       "
                    echo "| 2  --> zurück zu Geschichte            "
                    echo "| 99 --> Hauptmenü                       "
                    echo "+----------------------------------------+"
                    read -p "Deine Eingabe > " auswahl
                    if [[ $auswahl == 99 ]]; then statemachine=1; return $statemachine; fi
                    if [[ $auswahl == 2 ]]; then statemachine=4; return $statemachine; fi
                    if [[ $auswahl == 1 ]]; then statemachine=5;return $statemachine; fi
                    if [[ $auswahl =~ ^$varAbisZ ]]; then lesenlesen=$(< "$geschichtenPfad""$benutzerTagebuch"/"$auswahl"); echo $lesenlesen; fi
                    read "Taste drücken "; statemachine=6; return $statemachine
                   }

# Statemachine 11
administrator() {
                 echo "+--------------------------------------------+"
                 echo "| Bitte melden Sie sich als Administartor an  "
                 echo "| Du hast zwei Versuche bis du gesperrt wirst "
                 echo "| Abbrechen mit 'Q'                           "
                 echo "+--------------------------------------------+"
                 read -p "Deine Eingabe > " logAdministrator
                 adminORnot=$(grep -o "$logAdministrator" "$adminChef" | wc -l)
                 if [[ "$logAdministrator" =~ ^([qQ])$ ]] || [[ "$adminORnot" == "0" ]]; then statemachine=7; 
                    echo "Login falsch ... "
                    logAdministrator="leere Eingabe" ## Die Variable mit Siff füllen
                    read
                    return $statemachine;
                   else
                    echo "+--------------------------------------------+"
                    echo "| Du bist nun Administartor                   "
                    echo "| "
                    echo "| Wähle aus folgenden möglichkeiten aus       "
                    echo "|"
                    echo "| 1  --> Einen Benutzer löschen               "
                    echo "| 2  --> "
                    echo "| Q  --> Als Administartor abmelden           "
                    echo "+--------------------------------------------+"
                    read -p "Deine Eingabe > " admineingabe
                     if [[ $admineingabe =~ ^([qQ])$ ]]; then statemachine=7
                      echo "Du hast dich als Administartor abgemeldet ... "
                      admineingabe = "leere Variable" ## Variable mit Siff füllen
                      read
                      return $statemachine
                     fi ## Abmelden
                      if [[ $adminORnot == 1 ]]; then
                       echo "Welchen Benutzer soll gelöscht werden? Oder [A] für abbrechen"
                        read -p "Benutzer > " admineingabe
                        if [[ $admineingabe =~ ^([aA])$ ]]; then statemachine=11; return statemachine
                         else "Hier den Benutzer löschen"
                          read -p
                          statemachine=11
                        fi
                       return $statemachine
                      fi
                 fi
                 }

# Statemachine - eine hilfsfunktion
varAbisZ() {
            ([aA]|[bB]|[cC]|[dD]|[eE]|[fF]|[gG]|[hH]|[iI]|[jJ]|[kK]|[lL]|[mM]|[nN]|[oO]|[pP]|[qQ]|[rR]|[sS]|[tT]|[uU]|[vV]|[wW]|[xX]|[yY]|[zZ])
           }

# Statemachine - Eine hilfsfunktion
varNullBisNeun() {
                  ([1]|[2]|[3]|[4]|[5]|[6]|[7]|[8]|[8]|[9]|[0])
                 }

# Statemachine - gehört zu lesen
geschichteAuflisten() {
                      varAnzDat="$(ls "$geschichtenPfad""$benutzerTagebuch" | wc -l)"
                      varDatList="$(ls "$geschichtenPfad""$benutzerTagebuch")"
                      echo "| Geschichten auflisten                  "
                      echo "| Den Namen der Geschichte eintippen     "
                      echo "+----------------------------------------+"
                      if [[ $varAnzDat < 1 ]]; then echo "| Ordner ist leer";fi
                      if [[ $varAnzDat > 0 ]];then echo "| "$varDatList; fi
                      echo "+----------------------------------------+"
                     }

# Statemachine 
beenden() {
            echo "| Möchten Sie das Programm beenden?      "
            echo "| Wähle J / N oder Ja | Nein             "
            echo "+----------------------------------------+"
            read -p "| Eingabe > " auswahl
            if [[ $auswahl =~ ^([jJ][aA]|[jJ])$ ]]; then break; clear; fi
            if [[ $auswahl =~ ^([nN][eE][iI][nN]|[nN])$ ]]; then statemachine=7; fi
            return $statemachine
           }

##Statemaschine
# Variable der Statemachine
statemachine=7 # Direkt ins LogIn Fenster
#
while [ 0 ]; do
     clear
   case "$statemachine" in
     1)
       hauptmenue
        ;;
     4)
       geschichte
        ;;
     4.1)
       geschichteLoeschen
        ;;
     4.2)
       geschichteUmnennen
        ;;
     5)
       geschichteSchreiben
        ;;
     6)
       geschichteLesen
        ;;
     7)
       loginFenster
        ;;
     8)
       loginLogin
        ;;
     9)
       loginRegistrieren
        ;;

     10)
        benutzerLoeschen
        ;;
     11)
        administrator
         ;;
     99)
        beenden #Funktion beenden()
        ;;
     *)
        echo "Ungültige Auswahl, Sie werden zum Hauptmenü umgeleitet!"
        read # -p "Bitte eine Taste drücken"
        statemachine=7
        ;;
   esac
done
#
